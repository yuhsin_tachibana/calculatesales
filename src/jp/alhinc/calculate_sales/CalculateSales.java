package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String OVER_TEN_DIGITS = "合計金額が10桁を超えました";
	private static final String NOT_CONSECUTIVE_NUMBERS = "売上ファイル名が連番になっていません";
	private static final String INVALID_FORMAT = "のフォーマットが不正です";
	private static final String INVALID_BRANCH_CODE = "の支店コードが不正です";
	private static final String INVALID_COMMODITY_CODE = "の商品コードが不正です";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityName = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 正規表現、支店コード
		String branchCodeRegex = "^[0-9]{3}$";
		// 正規表現、商品コード
		String commodityCodeRegex = "^[a-z|A-Z|0-9]{8}$";

		//エラー処理、コマンドライン引数が1つ設定されているか
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "支店", branchCodeRegex)) {
			return;
		}

		// 商品定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityName, commoditySales, "商品", commodityCodeRegex)) {
			return;
		}

		// 売上ファイル読み込み処理
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//00000000.rcdという形式のファイルを抽出
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//エラー処理、売上ファイルが連番になっているか
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if((latter - former) != 1) {
				System.out.println(NOT_CONSECUTIVE_NUMBERS);
				return;
			}
		}

		//売上ファイルのデータををファイル単位でそれぞれのMapに格納
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;
				//売上ファイルのファイル名
				String fileName = rcdFiles.get(i).getName();

				//売上データを格納する
				List<String> rcdValues = new ArrayList<String>();

				//1行ごとにデータを取り出す
				while((line = br.readLine()) != null) {
					rcdValues.add(line);
				}

				//エラー処理、売上ファイルのデータフォーマットが正しいか
				if(rcdValues.size() != 3) {
					System.out.println(fileName + INVALID_FORMAT);
					return;
				}

				// エラー処理、売上ファイルの支店コードが支店定義ファイルに存在するか
				if(!branchNames.containsKey(rcdValues.get(0))) {
					System.out.println(fileName + INVALID_BRANCH_CODE);
					return;
				}

				// エラー処理、売上ファイルの商品コードが商品定義ファイルに存在するか
				if(!commodityName.containsKey(rcdValues.get(1))) {
					System.out.println(fileName + INVALID_COMMODITY_CODE);
					return;
				}

				// エラー処理、売上データが数字かどうか
				if(!rcdValues.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				// 売上データの抽出
				long fileSale = Long.parseLong(rcdValues.get(2));
				Long branchSaleAmount = branchSales.get(rcdValues.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(rcdValues.get(1)) + fileSale;

				// エラー処理、売り上げが10桁以下になっているか
				if(branchSaleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println(OVER_TEN_DIGITS);
					return;
				}

				// 支店売上データの更新
				branchSales.put(rcdValues.get(0), branchSaleAmount);

				// 商品売上データの更新
				commoditySales.put(rcdValues.get(1), commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityName, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> mapNames,
										Map<String, Long> mapSales,String fileType, String regex) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

            //エラー処理、定義ファイルが存在しない
			if(!file.exists()) {
				System.out.println(fileType + FILE_NOT_EXIST);
				return false;

			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// items[0]にコード、items[1]に名前
				String[] items = line.split(",");

				//エラー処理、定義ファイルのフォーマット異常
				if((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(fileType + FILE_INVALID_FORMAT);
					return false;
				}

				mapNames.put(items[0], items[1]);
				mapSales.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> mapNames, Map<String, Long> mapSales) {
		// 売上集計データの出力処理
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//支店売上データの書き出し
			for(String key : mapNames.keySet()) {
				bw.write(key + "," + mapNames.get(key) + "," + mapSales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
